Approaches to humour generation, using:
- [Google TensorFlow](MEWS/mewslib/codecs/)
- [SMURF](https://gitlab.com/FillCleaver/jokebot/-/tree/master/vendor/SMURF) (Slurp Mash-Up with Recursive Filtering) fed from twitter, theonion, Washington Times
- [SNOBOL/SPITBOL](https://github.com/spitbol/x64)
- [staq](https://github.com/softwareqinc/staq) quantum simulation library

Under development.  See [twitter feed](https://twitter.com/FillCleaver) for a curated selection of output.
